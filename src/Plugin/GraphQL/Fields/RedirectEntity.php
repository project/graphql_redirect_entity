<?php

namespace Drupal\graphql_redirect_entity\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Custom field that expose the Redirect entity in RedirectUrl types.
 *
 * @GraphQLField(
 *   id = "graphql_redirect_entity",
 *   secure = true,
 *   name = "entity",
 *   type = "Redirect",
 *   nullable = false,
 *   parents = {"RedirectUrl"}
 * )
 */
class RedirectEntity extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function isLanguageAwareField() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value;
  }

}
