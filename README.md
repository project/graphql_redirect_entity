# GraphQL Redirect Entity

GraphQL Redirect Entity simply makes possible to get a Redirect entity through a GraphQL query, while GraphQL core module will return NULL and other modules, like GraphQL Redirect will directly return the final content.

This way a front-end implementation can be fully aware that the requested content was target of a redirect by Drupal.
